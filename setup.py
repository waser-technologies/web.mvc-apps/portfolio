#from setuptools import setup, find_packages

#from djangocms_portfolio import __version__

import warnings

from distutils.core import setup

package = __import__('djangocms_portfolio')

try:
    README = open('README.md').read()
except:
    warnings.warn('Could not read README.md')
    README = None

try:
    REQUIREMENTS = open('requirements.txt').read()
except:
    warnings.warn('Could not read requirements.txt')
    REQUIREMENTS = None


CLASSIFIERS = [
    'Development Status :: 5 - Production/Stable',
    'Environment :: Web Environment',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: 3.8',
    'Framework :: Django',
    'Framework :: Django :: 1.11',
    'Framework :: Django CMS',
    'Framework :: Django CMS :: 3.4',
    'Framework :: Django CMS :: 3.5',
    'Framework :: Django CMS :: 3.6',
    'Framework :: Django CMS :: 3.7',
    'Topic :: Internet :: WWW/HTTP',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    'Topic :: Software Development',
    'Topic :: Software Development :: Libraries',
]


setup( 
        name="djangocms-portfolio",
        version=package.__version__,
        author='Waser Technologies',
        author_email='danny@waser.tech',
        url='https://gitlab.waser.tech/web.mvc-apps/portfolio',
        description='Showcase your work with django-cms.',
        long_description=open('README.md').read(),
        packages=[
            'djangocms_portfolio',
            'djangocms_portfolio.migrations'
        ],
        include_package_data=True,
        install_requires=REQUIREMENTS,
        classifiers=CLASSIFIERS,
)