from .models import PortfolioPlugin
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from django.conf import settings 
from django.utils.translation import ugettext as _

class PortfolioPluginBase(CMSPluginBase):
    model = PortfolioPlugin
    render_template = 'djangocms_portfolio/plugins/portfolio_list.html'
    name = _("Portfolio")

    def render(self, context, instance, placeholder):
        context = super(PortfolioPluginBase, self).render(context, instance, placeholder)

        return context

plugin_pool.register_plugin(PortfolioPluginBase)