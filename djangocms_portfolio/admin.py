from django.contrib import admin
from django.db import models
from cms.admin.placeholderadmin import FrontendEditableAdminMixin, PlaceholderAdminMixin
from parler.admin import TranslatableAdmin

from .models import WorkType, Project

class WorkTypeAdmin(TranslatableAdmin):
    pass

class ProjectAdmin(PlaceholderAdminMixin, FrontendEditableAdminMixin, TranslatableAdmin):
    pass


admin.site.register(WorkType, WorkTypeAdmin)
admin.site.register(Project,ProjectAdmin)
