from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.utils.translation import get_language
from parler.views import TranslatableSlugMixin, ViewUrlMixin
from django.views.generic import DetailView, ListView

from .models import Project, WorkType

class BasePortfolioView(ViewUrlMixin):
    model = Project

    def get_view_url(self):
        if not self.view_url_name:
            raise ImproperlyConfigured(
                'Missing `view_url_name` attribute on {0}'.format(self.__class__.__name__)
            )

        url = reverse(self.view_url_name, args=self.args, kwargs=self.kwargs)
        return self.request.build_absolute_uri(url)

class BaseWorkView(ViewUrlMixin):
    model = WorkType

class BaseWorkListView(BaseWorkView):
    context_object_name = 'work_list'
    template_name = 'djangocms_portfolio/work_list.html'
    view_url_name = 'djangocms_portfolio:work-list'

class BasePortfolioListView(BasePortfolioView):
    context_object_name = 'portfolio_list'
    template_name = 'djangocms_portfolio/portfolio_list.html'
    view_url_name = 'djangocms_portfolio:portfolio-list'

    def get_context_data(self, **kwargs):
        context = super(BasePortfolioListView, self).get_context_data(**kwargs)
        return context

class PortfolioListView(BasePortfolioListView, ListView):
    pass

class PortfolioDetailView(BasePortfolioView, DetailView):
    context_object_name = 'portfolio'
    template_name = 'djangocms_portfolio/portfolio_detail.html'
    #slug_field = 'slug'
    pk_url_kwarg = 'project_id'
    view_url_name = 'djangocms_portfolio:portfolio-detail'

    def get_context_data(self, **kwargs):
        context = super(PortfolioDetailView, self).get_context_data(**kwargs)
        context['meta'] = self.get_object().as_meta()
        return context

class WorkListView(BaseWorkListView, ListView):
    pass

class WorkDetailView(TranslatableSlugMixin, BaseWorkView, DetailView):
    pass