# -*- coding: utf-8 -*-
from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.utils.urlutils import admin_reverse
from django.urls import reverse
from django.utils.translation import override, ugettext_lazy as _

@toolbar_pool.register
class PortfolioToolbar(CMSToolbar):
    
    def populate(self):

        if not self.is_current_app:
            return

        admin_menu = self.toolbar.get_or_create_menu('djangocms_portfolio', _('Portfolio'))
        with override(self.current_lang):
            url = admin_reverse('djangocms_portfolio_project_add')
            admin_menu.add_modal_item(_('Add portfolio'), url=url)