from django.utils.translation import ugettext as _
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

@apphook_pool.register
class PortaFolioApp(CMSApp):
    name = _("Portfolio")
    app_name = 'djangocms_portfolio'

    def get_urls(self, page=None, language=None, **kwargs):
        return ["djangocms_portfolio.urls"]