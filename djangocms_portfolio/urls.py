from django.conf.urls import url
from . import views

app_name = "djangocms_portfolio"

urlpatterns = [
    url(r"^$", views.PortfolioListView.as_view(), name="portfolio-list"),
    url(r"^detail/(?P<work_slug>[-\w]+)/(?P<project_id>[0-9]+)/$", views.PortfolioDetailView.as_view(), name="portfolio-detail"),
    url(r'works/$', views.WorkListView.as_view(), name="work-list"),
    url(r"^works/detail/(?P<slug>[-\w]+)/$", views.WorkDetailView.as_view(), name="work-detail"),
]
