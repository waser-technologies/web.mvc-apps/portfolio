# coding: utf-8
from django.db import models
from cms.models import CMSPlugin, PlaceholderField
from django.core.exceptions import ValidationError
from django.utils.translation import get_language, ugettext, ugettext_lazy as _
from django.urls import reverse
from parler.models import TranslatableModel, TranslatedFields
from meta.models import ModelMeta
from filer.fields.image import FilerImageField

from .settings import get_setting

class WorkType(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(verbose_name=_("name"), max_length=100),
        slug = models.SlugField(),
        meta_description=models.TextField(
            verbose_name=_('project meta description'), blank=True, default=''
        ),
        meta_keywords=models.TextField(verbose_name=_('project meta keywords'),
                                       blank=True, default=''),
        meta_title=models.CharField(verbose_name=_('project meta title'),
                                    help_text=_('used in title tag and social sharing'),
                                    max_length=2000,
                                    blank=True, default=''),
        meta={'unique_together': (('language_code', 'slug'),)}
    )

    class Meta:
        verbose_name = _('work type')
        verbose_name_plural = _('work types')

    def __str__(self):
        default = ugettext('Work (no translation)')
        return self.safe_translation_getter('name', any_language=True, default=default)


class Project(TranslatableModel, ModelMeta):
    work = models.ForeignKey(WorkType, verbose_name=_("Work type"), related_name="Projects", on_delete=models.CASCADE)
    translations = TranslatedFields(
        meta_description=models.TextField(
            verbose_name=_('project meta description'), blank=True, default=''
        ),
        meta_keywords=models.TextField(verbose_name=_('project meta keywords'),
                                       blank=True, default=''),
        meta_title=models.CharField(verbose_name=_('project meta title'),
                                    help_text=_('used in title tag and social sharing'),
                                    max_length=2000,
                                    blank=True, default=''),
    )

    main_image = FilerImageField(verbose_name=_('main image'), on_delete=models.CASCADE)
    sequence = models.IntegerField(verbose_name=_('sequence'))
    content = PlaceholderField('project_content', related_name='project_content')

    class Meta:
        ordering = ('sequence',)
        verbose_name = _('portfolio project')
        verbose_name_plural = _('portfolio projects')


    def get_absolute_url(self, lang=None):
        if not lang or lang not in self.get_available_languages():
            lang = get_language()
        if not lang or lang not in self.get_available_languages():
            lang = self.get_current_language()
        if self.has_translation(lang):
            work_slug = self.work.safe_translation_getter('slug', language_code=lang)
            project_id = self.pk
            return reverse('djangocms_portfolio:portfolio-detail', kwargs={'work_slug': work_slug, 'project_id': project_id})
        # in case category doesn't exist in this language, gracefully fallback
        # to portfolio list.
        return reverse('djangocms_portfolio:portfolio-list')
     
    def __str__(self):
        default = ugettext('Portoflio (no translation)')
        return self.work.safe_translation_getter('name', any_language=True, default=default)

    def thumbnail_options(self):
        return get_setting('IMAGE_THUMBNAIL_SIZE')

    def full_image_options(self):
        return get_setting('IMAGE_FULL_SIZE')

class PortfolioPlugin(CMSPlugin):
    title=models.CharField(_('title'), max_length=752, blank=True)
    description=models.TextField(verbose_name=_("description"), blank=True)
    portfolio = models.ManyToManyField(Project)

    def copy_relations(self, oldinstance):
        self.portfolio = oldinstance.portfolio.all()