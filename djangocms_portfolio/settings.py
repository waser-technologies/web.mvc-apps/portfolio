def get_setting(name):
    from django.conf import settings

    default = {
        'PORTFOLIO_IMAGE_THUMBNAIL_SIZE': getattr(settings, 'PORTFOLIO_IMAGE_THUMBNAIL_SIZE', {
            'size': '120x120',
            'crop': True,
            'upscale': False
        }),

        'PORTFOLIO_IMAGE_FULL_SIZE': getattr(settings, 'PORTFOLIO_IMAGE_FULL_SIZE', {
            'size': '640x120',
            'crop': True,
            'upscale': False
        }),
    }
    return default['PORTFOLIO_%s' % name]