from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PortfolioAppConfig(AppConfig):
    name = 'djangocms_portfolio'
    verbose_name = _('django CMS Portfolio')