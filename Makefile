include ./.env

env:
	pip install -r './requirements.txt'

clone:	
	git clone $(GIT_PATH) .

pull:
	git pull $(GIT_PATH)

init:
	python ./manage.py migrate --no-input
	python ./manage.py collectstatic --no-input
	python ./manage.py createsuperuser

migrations:
	python ./manage.py makemigrations --no-input

migrate:
	python ./manage.py migrate --no-input

static:
	python ./manage.py collectstatic --no-input

messages:
	python ./manage.py makemessages -l fr --ignore=static/* --ignore=env/*

translate:
	python ./manage.py compilemessages -l fr

server:
	python ./manage.py runserver
